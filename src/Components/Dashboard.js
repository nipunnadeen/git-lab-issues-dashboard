import React, { Component } from 'react';
import axios from 'axios';
import DonutChart from 'react-donut-chart';
import "../CSS/dashboard.css";



class Dashboard extends Component {
  state = {
    all: 0,
    progress: 0,
    waiting: 0,
    entgra: 0
  };


  componentDidMount = () => {

      this.getData();
        setInterval(this.getData, 300000);

  }


  getData = () => {

    //Your Access Token
    const handle = ''; 
 
    axios.get('https://gitlab.com/api/v4/groups/entgra-customer-support/issues_statistics?labels=Waiting%20on%20Client&private_token='+handle)
      .then(res => {

        const statistics = res.data.statistics;
        console.log(res);
        this.setState({ waiting: statistics.counts.opened });

      })
      .catch(function (error) {
        console.log("hhh", error);
      });


    axios.get('https://gitlab.com/api/v4/groups/entgra-customer-support/issues_statistics?labels=In%20Progress&private_token='+handle)
      .then(res => {

        const statistics1 = res.data.statistics;
        console.log(res);
        this.setState({ progress: statistics1.counts.opened });

      })
      .catch(function (error) {
        console.log("hhh", error);
      });


    axios.get('https://gitlab.com/api/v4/groups/entgra-customer-support/issues_statistics?labels=Waiting%20on%20Entgra&private_token='+handle)
      .then(res => {

        const statistics2 = res.data.statistics;
        console.log(res);
        this.setState({ entgra: statistics2.counts.opened });

      })
      .catch(function (error) {
        console.log("hhh", error);
      });


      axios.get('https://gitlab.com/api/v4/groups/entgra-customer-support/issues_statistics?private_token='+handle)
      .then(res => {

        const statistics3 = res.data.statistics;
        console.log(res);
        this.setState({ all: statistics3.counts.opened });

      })
      .catch(function (error) {
        console.log("hhh", error);
      });

  }

  

  render() {

    return (
      <div className="dashboard">
        <h1>IoT Issues</h1>
        <div className="total">
          <div><b>Totall Issues: {this.state.all}</b></div>
        </div>
        <div className="chart"><DonutChart
          data={[{
            label: 'Opened',
            value: this.state.all-this.state.waiting-this.state.progress-this.state.entgra,

          },
          {
            label: 'Waiting on Entgra',
            value: this.state.entgra,
        
          },
          {
            label: 'In progress',
            value: this.state.progress,

          },
          {
            
            label: 'Waiting on Client',
            value: this.state.waiting,

          }]}

          colors={['blue', 'red', 'green','gold']}
        //   height={550}
        //   width={750}
        />
        </div>


      </div>

    );
  }
}


export default Dashboard;

